package com.FileHandlingDemo;
import java.io.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Example2 {

	public static void main(String[] args) {
		File file = new File("E:\\Test\\demo.txt");
		String content = "lets append the file";
	try {
		FileOutputStream fop = new FileOutputStream(file,true);
		if(!file.exists())
		{
			file.createNewFile();
		}
		{
			byte[] contentInBytes = content.getBytes();
			fop.write(contentInBytes);
			fop.close();
			System.out.println("done writting");
		}
	}
	
	catch(IOException e)
	{
		e.printStackTrace();

	}

}

}