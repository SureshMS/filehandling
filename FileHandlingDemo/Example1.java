package com.FileHandlingDemo;
import java.io.*;

public class Example1 {
	public static void main(String[] args) {
		try {
			FileInputStream fin = new FileInputStream("E:\\Test\\demo1.txt");
			int x = fin.read();
			while(x!=-1)//EOF
			{
				System.out.print((char)x);
				x=fin.read();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();

		}

	}

}
