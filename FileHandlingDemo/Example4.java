package com.FileHandlingDemo;
import java.io.*; 
public class Example4 {

	public static void main(String[] args) {
		{ 
	        File file = new File("E://Test//demo.txt"); 
	        FileInputStream fileStream = null;
			try {
				fileStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	        InputStreamReader input = new InputStreamReader(fileStream); 
	        BufferedReader reader = new BufferedReader(input); 
	          
	        String line; 
	          
	        // Initializing counters 
	        int countWord = 0; 
	        int sentenceCount = 0; 
	        int characterCount = 0; 
	               
	        // Reading line by line from the  
	        // file until a null is returned 
	        	try {
					while((line = reader.readLine()) != null) 
					{ 
					    if(line.equals("")) 
					    { 
					        countWord++; 
					    } 
					    if(!(line.equals(""))) 
					    { 
					          
					        characterCount += line.length(); 
					          
					        // \\s+ is the space delimiter in java 
					        String[] wordList = line.split("\\s+"); 
					          
					        countWord += wordList.length; 
					        characterCount += countWord -1; 
					          
					        // [!?.:]+ is the sentence delimiter in java 
					        String[] sentenceList = line.split("[!?.:]+"); 
					          
					        sentenceCount += sentenceList.length; 
					    } 
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	              
	            System.out.println("Total word count = " + countWord); 
	            System.out.println("Total number of sentences = " + sentenceCount); 
	            System.out.println("Total number of characters = " + characterCount); 
	           
	        } 
	    } 

	}


