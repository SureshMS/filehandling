package com.FileHandlingDemo;
import java.io.*;

public class Example3 {
public static void main(String[] args)
{
	File file = new File("E:\\Demo2.txt");
	if(!file.exists())
		try {
			file.createNewFile();
		}
	catch (IOException e)
	{
		e.printStackTrace();
	}
	FileWriter writer;
	try {
		writer = new FileWriter(file);
		writer.write("This is an example\n will not execute");
		writer.close();
	}
	catch (IOException e)
	{
		e.printStackTrace();
	}
	try {
		FileReader fr = new  FileReader ("E:\\Demo2.txt");
		char[] a =new char [50];
		fr.read(a);
		for(char c:a)
			System.out.println(c);
		fr.close();
		
	}
	catch (FileNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (IOException e)
	{
		e.printStackTrace();
	}
		
	}
}

